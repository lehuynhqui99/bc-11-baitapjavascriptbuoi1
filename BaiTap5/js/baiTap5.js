const tinhTongKySoBtn = document.getElementById('tinhTongKySo');
const number = document.getElementById('number');
const tongKySo = document.getElementById('tongKySo');

tinhTongKySoBtn.addEventListener('click', e => {
    if (number.value%1 !== 0 || number.value === '' || number.value <= 0 || number.value >=100) {
        alert('Giá trị nhập vào phải là số nguyên dương có từ 1 đến 2 chữ số (từ 1-99). Hãy nhập lại!');
        e.preventDefault();
    } else {
        const ten = Math.floor(number.value/10);
        const unit = number.value%10;

        //Sử dụng toán tử ba ngôi để nếu ngừoi dùng nhập vào số có 1 chữ số thì
        // nó sẽ tụe chuyển qua dạng có 2 chữ số (gắn 0 vào đầu)
        // -> bài toán chặt chẽ hơn
        ten === 0 
        ?tongKySo.innerHTML = `Số 0${number.value} có tổng hai ký số là: <span class='text-danger'>${ten + unit}</span>`
        : tongKySo.innerHTML = `Số ${number.value} có tổng hai ký số là: <span class='text-danger'>${ten + unit}</span>`;

        e.preventDefault();
        
    }
});
