const no1 = document.getElementById('no1');
const no2 = document.getElementById('no2');
const no3 = document.getElementById('no3');
const no4 = document.getElementById('no4');
const no5 = document.getElementById('no5');

const tinhTrungBinhBtn = document.getElementById('tinhTrungBinh');

const trungBinh = document.getElementById('trungBinh');


tinhTrungBinhBtn.addEventListener ('click', e => {
    if (no1.value === "" || no2.value === "" || no3.value === "" || no4.value === "" || no5.value === "" ) {
        //ngăn chặn form submit
        e.preventDefault();

        //hiẻn thị thông báo bắt buộc nguời dùng nhập giá trị
        alert('Hãy nhập tất cả số mà bạn muốn tính trung bình, tất cả các trường phải là số. Hãy nhập lại!');
    } else {
        e.preventDefault();

        trungBinh.innerHTML = 
        `Giá trị trung bình là: <span class='text-danger'> ${(Number(no1.value) + Number(no2.value) + Number(no3.value) + Number(no4.value) + Number(no5.value))/5} </span>`;
    }
}) ;