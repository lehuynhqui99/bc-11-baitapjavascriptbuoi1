const dai = document.getElementById('dai');
const rong = document.getElementById('rong');
const tinhCSBtn = document.getElementById('tinhCS');
const tinhChuViDienTich = document.getElementById('tinhChuViDienTich');

tinhCSBtn.addEventListener('click', e => {
    if (dai.value === '' || rong.value === '' || dai.value <= 0 || rong.value <= 0) {
        alert('Hãy nhập giá trị cho chiều dài và chiều rộng của hình chữ nhật, giá trị nhập vào phải là số lớn hơn 0. Hãy nhập lại!');
        e.preventDefault();
    } else {
        tinhChuViDienTich.innerHTML = 
        `Chu vi của hình chữ nhật là: <span class='text-danger'>${(Number(dai.value) + Number(rong.value))*2}</span> (unit) <br>
        Diện tích của hình chữ nhật là: <span class='text-danger'>${dai.value * rong.value}</span> (unit<sup>2</sup>)`;
        e.preventDefault();
    }
});