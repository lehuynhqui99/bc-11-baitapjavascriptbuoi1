const ngayLuong = document.getElementById('ngayLuong');
const tinhLuongBtn = document.getElementById('tinhLuong');
const tienLuong = document.getElementById('tienLuong');
const dauPhay = document.getElementById('dauPhay');

const luong1Ngay =  100000;

const formatter = new Intl.NumberFormat('vn-VN');

tinhLuongBtn.addEventListener ('click', e => {
    if (ngayLuong.value === "" || ngayLuong.value <= 0) {
        //ngăn chặn form submit
        e.preventDefault();

        //hiẻn thị thông báo bắt buộc nguời dùng nhập giá trị
        alert('Hãy nhập số ngày làm của bạn và số ngày làm phải là số lớn hơn 0. Hãy nhập lại!');
    } else {
        e.preventDefault();

        //Xử lý người dùng muốn dùng dấu "," để ngăn cách số tiền hay không 
        // bằng toán tử 3 ngôi và in ra kết quả
        dauPhay.checked 
        ? tienLuong.innerHTML = `Tiền lương tháng này của bạn là: <span class='text-danger'> ${formatter.format(Number(ngayLuong.value) * luong1Ngay)} VND </span>` 
        : tienLuong.innerHTML = `Tiền lương tháng này của bạn là: <span class='text-danger'> ${Number(ngayLuong.value) * luong1Ngay} VND </span>`;
    }
});